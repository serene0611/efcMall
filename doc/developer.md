## 开发者文档
---

### 包结构说明

### 扩展实现

### 开发规范

#### [Git提交规范](git_commit.md)

#### SQL约束

#### 方法命名约束
* Dao:
  * load*: 返回的值需要用`java.util.Optional`包装，表示结果可能为null
  * get*: 返回值不会为null
* Mapper:
  * fetch*: 表示延迟加载对象，不会在程序中直接调用

### 技术

#### 基础

* Spring
* Spring Boot

#### 重试框架

* Spring Retry

#### 表达式语言

* SpEL

#### 缓存数据库

* Redis
* Redisson
* Spring Data Redis

### 数据库

* MySql
* Hikari
* MyBatis
* MyBatis Dynamic Sql
* MyBatis Generator

通过`MyBatis Generator`生成数据库实体类、Mapper接口、DynamicSqlSupport类

数据库实体类关联属性支持懒加载

> 懒加载的属性使用的Mapper方法必须以fetch开头，以区分非懒加载数据


#### 单元测试

* Junit5
* JMockit
* Mockito
* Spring TestContext Framework
* Spring Boot Test

单元测试基本要求（A-TRIP）

* 自动化（Automatic）：调用测试自动化和结果检查自动化
* 彻底的（Thorough）：语句覆盖率要求达到 70% ;核心模块的语句覆盖率和分支覆盖率要求达到 100%。
* 可重复（Repeatable）：测试必须可重复执行，并且产生相同的结果
* 独立的（Independent）：独立于其他测试和依赖环境
* 专业的（Professional）：测试类代码的质量应该与业务代码的质量一致

##### 最佳实践

[https://www.testcontainers.org/](https://www.testcontainers.org/)

[https://phauer.com/2017/dont-use-in-memory-databases-tests-h2/](https://phauer.com/2017/dont-use-in-memory-databases-tests-h2/)

###### MySql 数据库测试

推荐使用H2作为内嵌数据库，支持Mysql模式

###### Redis 缓存数据库测试
方式一：embedded-redis（内嵌方式）

```xml
<dependency>
  <groupId>it.ozimov</groupId>
  <artifactId>embedded-redis</artifactId>
  <version>0.7.2</version>
</dependency>
```

[https://github.com/ozimov/embedded-redis](https://github.com/ozimov/embedded-redis)

[https://github.com/kstyrc/embedded-redis](https://github.com/kstyrc/embedded-redis)

对于Window开发者，内嵌版本默认使用Redis 2.8版本，可以手动下载Redis可执行文件
[https://github.com/microsoftarchive/redis/releases](https://github.com/microsoftarchive/redis/releases)

方式二：redis-mock（Mock方式）

```xml
<dependency>
    <groupId>com.github.fppt</groupId>
    <artifactId>jedis-mocck</artifactId>
    <version>0.1.16</version>
</dependency>
```

[https://github.com/fppt/jedis-mock](https://github.com/fppt/jedis-mock)

[https://github.com/zxl0714/redis-mock](https://github.com/zxl0714/redis-mock)

目前我们将使用方式一

#### 其他

* Guava
* SnakeYaml
* Jackson


