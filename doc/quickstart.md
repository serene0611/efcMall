## 快速开始

### 安装依赖
---
* (必须) Java运行环境
* (必须) [MySql数据库](https://www.mysql.com/downloads/)
* (必须) [Redis数据库](https://redis.io/download)

> 确保以上服务已正常启动，相关启动命令：
>
> Redis: redis/bin/redis-server redis/conf/redis.conf --daemonize yes
>
> Mysql: service mysqld start

### 初始化
---
* 建立数据库

```shell
[root@host]# mysql -u root -p
Enter password:******

mysql> CREATE DATABASE IF NOT EXISTS efcmall DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```

### 编译efcmall
---

以下是如何快速开始使用本项目的步骤：  
- **1，克隆项目到本地：**
```shell
git clone https://gitee.com/easyforcode/efcMall.git  
```

- **2，进入项目目录：**
```shell
cd efcMall  
```

- **3，编译和运行 admin 模块：**
```shell
mvn package -DskipTests  
mvn spring-boot:run
```

### 修改配置文件
---

```shell
> vim conf/efcmall.properties
```

####更新数据库配置

```properties
spring.datasource.url=jdbc:mysql://<IP>:<PORT>/sncflow?serverTimezone=UTC
spring.datasource.username=<username>
spring.datasource.password=<password>
```

####更新Redis配置

```properties
spring.redis.host=<IP>
spring.redis.port=6379
spring.redis.password=<password>
```

### 启动服务
---

```shell
> bin/nohup java -Xms512m -Xmx512m -Xmn256m -jar   efcmall.jar --server.port=8082 >  nohup.out 2>&1 & 
```

通过浏览器访问 [http://[IP]:8082/docs.html]([http://localhost:8520/docs.html)

如果正常，表示efcMall服务启动成功

## 常见问题  

* 关于端口占用问题  

问题描述：我在部署项目时遇到端口占用问题，控制台显示了端口已被占用的错误。我检查了相关配置，但无法解决该问题。您能提供一些解决方案吗？  
回答：端口占用问题通常是由其他应用程序在使用相同端口导致的。您可以使用命令行工具（例如netstat或lsof）来查找当前正在使用的端口。确保您的项目使用的端口没有被其他应用占用。如果端口已被占用，您可以尝试更改项目配置中的端口号，以避免冲突。此外，也可以通过终止占用该端口的应用程序来解决问题。  