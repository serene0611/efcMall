package com.mall.wechat.bean;

/**
 * {
 *     "errcode":0 ,
 *     "errmsg":"ok" ,
 *     "provider_access_token":"enLSZ5xxxxxxJRL",
 *     "expires_in":7200
 *  }
 */
public class TokenBean {
    private int errcode;
    private String errmsg;
    private String provider_access_token;
    /**
     * 预授权码,最长为512字节
     * CorporateLicense.get_pre_auth_code 使用
     */
    private String pre_auth_code;
    /**
     * 授权方（企业）access_token,最长为512字节
     * CorporateLicense.get_corp_token 使用
     */
    private String access_token;
    private int expires_in;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getProvider_access_token() {
        return provider_access_token;
    }

    public void setProvider_access_token(String provider_access_token) {
        this.provider_access_token = provider_access_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getPre_auth_code() {
        return pre_auth_code;
    }

    public void setPre_auth_code(String pre_auth_code) {
        this.pre_auth_code = pre_auth_code;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
