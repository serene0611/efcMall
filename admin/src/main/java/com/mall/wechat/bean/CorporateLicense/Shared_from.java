/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Shared_from {

    //共享了应用的互联企业信息，仅当企业互联或者上下游共享应用触发的安装时才返回
    private String corpid;
    public void setCorpid(String corpid) {
         this.corpid = corpid;
     }
     public String getCorpid() {
         return corpid;
     }

}