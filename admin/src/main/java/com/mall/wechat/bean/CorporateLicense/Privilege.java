/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;
import java.util.List;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Privilege {

    //权限等级。
    //1:通讯录基本信息只读
    //2:通讯录全部信息只读
    //3:通讯录全部信息读写
    //4:单个基本信息只读
    //5:通讯录全部信息只写
    private int level;
    //应用可见范围（部门）
    private List<Integer> allow_party;
    //应用可见范围（成员）
    private List<String> allow_user;
    //应用可见范围（标签）
    private List<Integer> allow_tag;
    //额外通讯录（部门）
    private List<Integer> extra_party;
    //额外通讯录（成员）
    private List<String> extra_user;
    //额外通讯录（标签）
    private List<Integer> extra_tag;
    public void setLevel(int level) {
         this.level = level;
     }
     public int getLevel() {
         return level;
     }

    public void setAllow_party(List<Integer> allow_party) {
         this.allow_party = allow_party;
     }
     public List<Integer> getAllow_party() {
         return allow_party;
     }

    public void setAllow_user(List<String> allow_user) {
         this.allow_user = allow_user;
     }
     public List<String> getAllow_user() {
         return allow_user;
     }

    public void setAllow_tag(List<Integer> allow_tag) {
         this.allow_tag = allow_tag;
     }
     public List<Integer> getAllow_tag() {
         return allow_tag;
     }

    public void setExtra_party(List<Integer> extra_party) {
         this.extra_party = extra_party;
     }
     public List<Integer> getExtra_party() {
         return extra_party;
     }

    public void setExtra_user(List<String> extra_user) {
         this.extra_user = extra_user;
     }
     public List<String> getExtra_user() {
         return extra_user;
     }

    public void setExtra_tag(List<Integer> extra_tag) {
         this.extra_tag = extra_tag;
     }
     public List<Integer> getExtra_tag() {
         return extra_tag;
     }

}