package com.mall.wechat.bean.CorporateLicense;

import java.util.List;

public class Admin_list {
    //错误码，0表示正常返回，可读取admin的管理员列表
    private int errcode;
    //错误说明
    private String errmsg;
    //应用的管理员列表（不包括外部管理员），成员授权模式下，不返回系统管理员
    private List<Admin> admin;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public List<Admin> getAdmin() {
        return admin;
    }

    public void setAdmin(List<Admin> admin) {
        this.admin = admin;
    }

    public class Admin {
        //管理员的userid
        private String userid;
        //管理员的open_userid
        private String open_userid;
        //该管理员对应用的权限：0=发消息权限，1=管理权限
        private String auth_type;


        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getOpen_userid() {
            return open_userid;
        }

        public void setOpen_userid(String open_userid) {
            this.open_userid = open_userid;
        }

        public String getAuth_type() {
            return auth_type;
        }

        public void setAuth_type(String auth_type) {
            this.auth_type = auth_type;
        }
    }
}
