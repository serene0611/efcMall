package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录token
 * <pre>
 *     自动生成代码: 表名 system_token
 *     id                 int(11)
 *     user_id            int(11)
 *     token              varchar(30)
 *     validity_time      datetime
 *     create_time        datetime
 * </pre>
 *
 * @author caven 2020-07-05 10:42:28
 */
public class SystemToken implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 自增id
	 */
	private Integer id;

	/** 
	 * 登录帐号
	 */
	private Integer userId;

	/** 
	 * 登录token
	 */
	private String token;

	/** 
	 * 有效期
	 */
	private Date validityTime;

	/** 
	 * 创建时间
	 */
	private Date createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getValidityTime() {
		return validityTime;
	}

	public void setValidityTime(Date validityTime) {
		this.validityTime = validityTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}