package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * 菜单权限表 
 * <pre>
 *     自动生成代码: 表名 system_menu_authorize
 *     id           int(11)
 *     user_id      int(11)
 *     menu_id      int(11)
 * </pre>
 *
 * @author caven 2022-01-04 17:59:40
 */
public class SystemMenuAuthorize implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 自增id
	 */
	private Integer id;

	/** 
	 * 用户唯一编码
	 */
	private Integer userId;

	/** 
	 * 菜单唯一编码
	 */
	private Integer menuId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}