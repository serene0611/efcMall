package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品属性值表
 * <pre>
 *     自动生成代码: 表名 goods_attribute_value
 *     id                   int(11)
 *     goods_info_code      varchar(80)
 *     attribute_code       varchar(80)
 *     attribute_value      varchar(255)
 *     status               tinyint(4)
 *     create_time          datetime
 *     update_time          datetime
 * </pre>
 *
 * @author caven 2020-07-10 17:48:49
 */
public class GoodsAttributeValue implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 自增id
	 */
	private Integer id;

	/** 
	 * 商品大类别表id
	 */
	private String goodsInfoCode;

	/** 
	 * 菜单唯一编码
	 */
	private String attributeCode;

	/** 
	 * 数值
	 */
	private String attributeValue;

	/** 
	 * 状态  1：启动  0：禁用
	 */
	private Integer status;

	/** 
	 * 创建时间
	 */
	private Date createTime;

	/** 
	 * 更新时间
	 */
	private Date updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoodsInfoCode() {
		return goodsInfoCode;
	}

	public void setGoodsInfoCode(String goodsInfoCode) {
		this.goodsInfoCode = goodsInfoCode;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}