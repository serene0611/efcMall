package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品属性表 sku表
 * <pre>
 *     自动生成代码: 表名 goods_attribute
 *     id                        int(11)
 *     goods_info_code           varchar(80)
 *     attribute_code            varchar(80)
 *     attribute_name            varchar(80)
 *     attribute_value_type      tinyint(4)
 *     is_single_value           tinyint(4)
 *     attribute_ype             tinyint(4)
 *     is_show                   tinyint(4)
 *     sort                      int(11)
 *     status                    tinyint(4)
 *     create_time               datetime
 *     update_time               datetime
 *     remark                    varchar(255)
 * </pre>
 *
 * @author caven 2020-07-10 17:48:32
 */
public class GoodsAttribute implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 自增id
	 */
	private Integer id;

	/** 
	 * 商品大类别表id
	 */
	private String goodsInfoCode;

	/** 
	 * 菜单唯一编码
	 */
	private String attributeCode;

	/** 
	 * 上级菜单编码
	 */
	private String attributeName;

	/** 
	 * 1：数字（价格） 2：数字（不带小数点） 3：数字（带小数点） 4：文本  5：下拉框  6：日期  7：密码框 
	 */
	private Integer attributeValueType;

	/** 
	 * 1：单个值  2：数组（多个值） 
	 */
	private Integer isSingleValue;

	/** 
	 * 1：系统内置不能删除，例如名称，价格，库存  2：主键 sku的属性  3：其他类别有关属性 
	 */
	private Integer attributeYpe;

	/** 
	 * 是否显示，attribute_ype为1和2必须显示  1：是  0：否
	 */
	private Integer isShow;

	/** 
	 * 排序 最大排前面
	 */
	private Integer sort;

	/** 
	 * 状态  1：启动  0：禁用
	 */
	private Integer status;

	/** 
	 * 创建时间
	 */
	private Date createTime;

	/** 
	 * 更新时间
	 */
	private Date updateTime;

	/** 
	 * 备注
	 */
	private String remark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoodsInfoCode() {
		return goodsInfoCode;
	}

	public void setGoodsInfoCode(String goodsInfoCode) {
		this.goodsInfoCode = goodsInfoCode;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public Integer getAttributeValueType() {
		return attributeValueType;
	}

	public void setAttributeValueType(Integer attributeValueType) {
		this.attributeValueType = attributeValueType;
	}

	public Integer getIsSingleValue() {
		return isSingleValue;
	}

	public void setIsSingleValue(Integer isSingleValue) {
		this.isSingleValue = isSingleValue;
	}

	public Integer getAttributeYpe() {
		return attributeYpe;
	}

	public void setAttributeYpe(Integer attributeYpe) {
		this.attributeYpe = attributeYpe;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}