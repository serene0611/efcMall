package com.mall.config;

import com.yomahub.tlog.id.TLogIdGenerator;

public class TraceIdGenerator extends TLogIdGenerator {
    @Override
    public String generateTraceId() {
        return String.valueOf("chok-top#"+System.nanoTime());
    }
}