package com.mall.login;

import com.mall.utils.encrypt.AESUtil;
import org.apache.commons.lang3.StringUtils;

public class Token {

    private final static String KEY="tsHvouTdp5XBB4c5cBEvfQ==";
    private final static String SEPARATOR="##";
    /**
     * token过期时间，单位秒
     */
    public final static int TIMEOUT= 60 * 30;


    /**
     * 获取token
     * @param businessCode 登录商家号
     * @param codeAccount 登录账号
     * @param userName 用户名
     * @param imei 登录Imei
     * @return
     */
    public static String genToken(String businessCode, String codeAccount, String userName, String imei) throws Exception {
        imei= StringUtils.isBlank(imei)?"-":imei;
        String token = businessCode + SEPARATOR + codeAccount + SEPARATOR + userName + SEPARATOR + imei + SEPARATOR + System.currentTimeMillis();
        return AESUtil.encrypt(token,KEY);
    }

    public static String[] decompileToken(String token) throws Exception {
        String tokenString = AESUtil.decrypt(token,KEY);
        if(StringUtils.isNotBlank(tokenString)){
            return tokenString.split(SEPARATOR);
        }else{
            return null;
        }
    }

    /**
     * 获取token的key，可以用于保存redis
     * @param businessCode 登录商家号
     * @param codeAccount 登录账号
     * @return
     */
    public static String getTokenKey(String businessCode, String codeAccount) throws Exception {
        return businessCode + SEPARATOR + codeAccount;
    }


    public static void main(String[] args) throws Exception {
        System.out.println(genToken("abcsjhdf","wfcaven","138fhs8dy8ewfwf8h8shff","34fh83ur:rer:Fwef;Wef;W4:"));
    }

}
